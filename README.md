# jupyter-cylc-ui-proxy
Integrate Cylc UI in the Jupyter environment.

## Requirements
- Python 3.8+
- Jupyter Notebook 6.0+
- JupyterLab 3.0+

This package executes the `cylc gui` command. This command assumes the `cylc` command is available in the environment's.

## Jupyter-Server-Proxy
[Jupyter-Server-Proxy](https://jupyter-server-proxy.readthedocs.io) lets you run arbitrary external processes (such as Tensorboard) alongside your notebook, and provide authenticated web access to them.

## Install

#### Create and Activate Environment
```
virtualenv -p python3 venv
source venv/bin/activate
```

#### Install jupyter-tensorboard-proxy
```
pip install git+https://gitlab.com/idris-cnrs/jupyter/jupyter-proxy-apps/jupyter-cylc-ui-proxy.git
```

#### Enable jupyter-server-proxy Extensions

For Jupyter Lab, install the @jupyterlab/server-proxy extension:
```
jupyter labextension install @jupyterlab/server-proxy
```
