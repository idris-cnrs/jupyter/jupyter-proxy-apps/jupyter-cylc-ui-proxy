"""Simple import and function call test"""


def test_setupcall():
    """
    Test the call of the setup function
    """
    import jupyter_cylc_ui_proxy as jt

    print("Running test_setupcall...")
    print(jt.setup_cylc_ui())
