# Copyright 2023 IDRIS / jupyter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Module to setup Cylc UI as Jupyter server Proxy app"""

import os
import logging
import shutil
import secrets

from typing import Any
from typing import Dict


def get_logger(name):
    """Configure logging"""
    logger = logging.getLogger(name)
    if not logger.handlers:
        # Prevent logging from propagating to the root logger
        logger.propagate = 0
        console = logging.StreamHandler()
        logger.addHandler(console)
        formatter = logging.Formatter(
            '[%(levelname).1s %(asctime)s.%(msecs)03d %(module)s '
            '%(funcName)s:%(lineno)d] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )
        console.setFormatter(formatter)
    return logger


def setup_cylc_ui() -> Dict[str, Any]:
    """ Setup commands and return a dictionary compatible
        with jupyter-server-proxy.
    """

    # Set logging
    logger = get_logger(__name__)
    logger.setLevel(logging.INFO)

    cylc_exec = 'cylc'

    # generate a random token to be used for UI server
    cylc_ui_token = str(secrets.token_hex(16))

    # Set a new jupyter server config dir
    jupyter_server_config_dir = os.path.join(
        os.environ['HOME'], '.cylc', 'etc', 'jupyter'
    )

    def _get_icon_path():
        """Get the icon path"""
        return os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'icons', 'cylc-logo.svg'
        )

    # launchers url file including url parameters
    def _get_path_info():
        """Get custom URL launcher with auth parmeters"""
        return f'cylc_gui/cylc?token={cylc_ui_token}'

    def _set_env_vars():
        """Set relevant env vars"""
        return {
            'JUPYTER_CONFIG_DIR': jupyter_server_config_dir
        }

    def _cylc_command(port, args):
        """Make cylc launch command"""
        # Check if cylc can be found on PATH
        executable = shutil.which(cylc_exec)
        if not executable:
            raise FileNotFoundError(
                f'{cylc_exec} executable not found. Please load '
                f'appropriate module before launching application'
            )

        # Ensure we create cylc jupyter server config dir
        os.makedirs(jupyter_server_config_dir, exist_ok=True)

        # Make cylc command arguments
        cmd_args = [
            cylc_exec, 'gui', '--ServerApp.ip=127.0.0.1',
            f'--ServerApp.port={port}',
            f'--ServerApp.token={cylc_ui_token}',
            '--no-browser',
        ]

        # If arguments like host, port are found in config, delete them.
        # We let Jupyter server proxy to take care of them
        # Additionally we delete path_prefix as well.
        for arg in ['--ServerApp.ip', '--ServerApp.port', '--ServerApp.token']:
            if arg in args:
                idx = args.index(arg)
                # These arguments will not have any values, so we need to just
                # remove them
                del args[idx]
        cmd_args += args

        logger.info(
            'Cylc UI will be launched with cmds %s', cmd_args
        )

        return cmd_args

    return {
        'command': _cylc_command,
        'environment': _set_env_vars(),
        'absolute_url': False,
        'timeout': 300,
        'new_browser_tab': True,
        'launcher_entry': {
            'enabled': True,
            'path_info': _get_path_info(),
            'icon_path': _get_icon_path(),
            'title': 'Cylc UI',
            'category': 'Applications',
        },
    }

from . import _version
__version__ = _version.get_versions()['version']
